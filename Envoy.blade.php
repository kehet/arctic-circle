@servers(['web' => 'deployer@kehet.com'])

@setup
    $repository = 'git@gitlab.com:kehet/arctic-circle.git';

    if($branch == 'master') :
        $releases_dir = '/home/arctic-circle-fi/arctic-circle/releases';
        $app_dir = '/home/arctic-circle-fi/arctic-circle';
    elseif ($branch == 'develop') :
        $releases_dir = '/home/beta-arctic-circle-fi/arctic-circle/releases';
        $app_dir = '/home/beta-arctic-circle-fi/arctic-circle';
    else:
        die('unknown branch');
    endif;

    $release = date('Y-m-d-H-i-s');
    $new_release_dir = $releases_dir .'/'. $release;
@endsetup

@story('deploy')
    clone_repository
    run_composer
    run_npm
    update_symlinks
    rebuild_cache
    publish_new_current
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    [ -d {{ $releases_dir }} ] || mkdir {{ $releases_dir }}
    git clone --depth 1 --branch {{ $branch }} {{ $repository }} {{ $new_release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment ({{ $release }})"
    cd {{ $new_release_dir }}

    echo "Building Composer"
    @if($branch == 'master')
        composer install --prefer-dist --no-scripts --no-dev -q -o
    @else
        composer install
    @endif

    php artisan vendor:publish --tag=horizon-assets
@endtask

@task('run_npm')
    echo "Building NPM"
    cd {{ $new_release_dir }}
    npm install --quiet
    npm run prod --quiet
@endtask

@task('update_symlinks')
    echo "Linking storage directory"
    rm -rf {{ $new_release_dir }}/storage
    ln -nfs {{ $app_dir }}/storage {{ $new_release_dir }}/storage

    echo 'Linking .env file'
    ln -nfs {{ $app_dir }}/.env {{ $new_release_dir }}/.env
@endtask

@task('rebuild_cache')
    echo "Migrate & Rebuild caches"
    cd {{ $new_release_dir }}
    php artisan migrate --force

    @if($branch == 'master')
        php artisan cache:clear
        # php artisan config:clear
        # php artisan route:clear
        # php artisan view:clear
        php artisan config:cache
        php artisan route:cache
        php artisan view:cache
    @endif

    php artisan storage:link
@endtask

@task('publish_new_current')
    echo 'Linking current release'
    ln -nfs {{ $new_release_dir }} {{ $app_dir }}/current

    @if($branch == 'master')
        cd {{ $new_release_dir }}
        echo 'Reboot php fpm to unlink old files'
        sudo /usr/sbin/service php7.2-fpm restart
        sudo /usr/bin/supervisorctl restart arctic-circle-fi-horizon
        sleep 5
        php artisan cache:warm-up
    @endif
@endtask
