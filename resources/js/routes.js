import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter);

export default new VueRouter({
    routes: [
        {
            path: '/',
            name: 'home',
            component: require('./pages/Home.vue')
        },
        {
            path: '/apply',
            name: 'apply',
            component: require('./pages/Apply.vue')
        },
        {
            path: '/applications/:id',
            name: 'applications.show',
            component: require('./pages/ApplicationsShow.vue')
        },
        {
            path: '/applications',
            name: 'applications',
            component: require('./pages/Applications.vue')
        },
        {
            path: '/roster',
            name: 'roster',
            component: require('./pages/Roster.vue')
        },
        {
            path: '/rules',
            name: 'rules',
            component: require('./pages/Rules.vue')
        },
    ]
});
