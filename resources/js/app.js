
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueAnalytics from 'vue-analytics'
import VueImg from 'v-img'
import router from './routes.js'

Vue.use(BootstrapVue);

Vue.use(VueAnalytics, {
    id: 'UA-125843770-1',
    router
});

Vue.use(VueImg);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('navigation-bar', require('./components/NavigationBar.vue'));
Vue.component('character-selector-modal', require('./components/CharacterSelectorModal.vue'));
Vue.component('blog-post-edit-modal', require('./components/BlogPostEditModal.vue'));
Vue.component('recruitment-status-edit-modal', require('./components/RecruitmentStatusEditModal.vue'));
Vue.component('apply-text-edit-modal', require('./components/ApplyTextEditModal.vue'));
Vue.component('rules-edit-modal', require('./components/RulesEditModal.vue'));
Vue.component('username', require('./components/Username.vue'));

const data = {
    user: window.store.user,
    links: window.store.links,
    loggedIn: (window.store.user !== null),
    updateUser() {
        window.axios.get('/api/profile')
            .then(response => {
                this.user = response.data.data.user;
            });
    }
};

new Vue({
    router,
    data
}).$mount('#app');
