<!doctype html>
<html lang="fi">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <meta name="description" content="Arctic Circle - Darkspear EU - Finnish WoW Gaming Community"/>
    <meta name="keywords" content="Arctic Circle,Darkspear,WoW,World of Warcraft,Gaming,Kilta"/>
    <meta name="author" content="Arctic Circle"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <!-- Browser Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('apple-touch-icon.png') }}"/>
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon-32x32.png') }}"/>
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon-16x16.png') }}"/>
    <link rel="manifest" href="{{ asset('site.webmanifest') }}"/>
    <meta name="msapplication-TileColor" content="#da532c"/>
    <meta name="theme-color" content="#ffffff"/>

    <!-- Facebook -->
    <meta property="og:image:width" content="240"/>
    <meta property="og:image:height" content="240"/>
    <meta property="og:title" content="Arctic Circle"/>
    <meta property="og:description" content="Arctic Circle - Darkspear EU - Finnish WoW Gaming Community"/>
    <meta property="og:url" content="https://arctic-circle.fi/"/>
    <meta property="og:image" content="https://arctic-circle.fi/og-image.jpg"/>

    <title>Arctic Circle - Darkspear EU - Finnish WoW Gaming Community</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Roboto:400,500" rel="stylesheet"/>

    <link rel="stylesheet" href="{{ mix('css/app.css') }}"/>

    <script>
        window.store = {
            user: @json($user),
            links: {
                login: "{{ route('login') }}",
                logout: "{{ route('logout') }}",
            }
        };
    </script>
</head>
<body>

<div id="app">

    <character-selector-modal ref="characterSelectorElement"></character-selector-modal>

    <div class="container text-center my-3">
        <img
            src="{{ asset(App::environment('production') ? 'images/arctic-circle-bare.png' : 'images/arctic-circle-bare-beta.png') }}"
            alt="Arctic Circle Logo" class="img-fluid"/>
    </div>

    <div class="container p-0">
        <navigation-bar></navigation-bar>
    </div>

    <div class="container container-background mb-5 py-4" style="min-height: 400px;">
        <!--[if lt IE 7]>
        <div class="alert alert-warning">
            <h1>Heitotanoin...</h1>
            <p>
                Valitettavasti tälläset moternit nettisivut ei toimi näin vanhalla selaimella. Vaikka
                <a href="https://www.mozilla.org/" target="_blank" rel="nofollow">Firefox</a> on ihan hyvä.
                En voi olla ensimmäinen joka tästä sulle valittaa.
            </p>
        </div>
        <![endif]-->
        <noscript>
            <div class="alert alert-warning">
                <h1>Heitotanoin...</h1>
                <p>
                    Valitettavasti tälläset moternit nettisivut ei toimi ilman Javascriptiä. Uskon kuitenkin vahvasti
                    että osaat sen omatoimisesti laittaa päälle tai tiedät ainakin keltä kysyä (ite varmaan oot sen
                    poiski ottanu, ei näitä ny minkään firman puolesta luulisi pois otettavan (miksi?)).
                    <a href="https://www.enable-javascript.com/" target="_blank" rel="nofollow">Tälläsen löysin nopeesti googlesta</a>
                    mutta vaikea uskoa että tuosta nyt mitään oikeeta hyötyä kellekkään on mutta vissiin tapana
                    tällänen aina laittaa näkyviin skriptittömille. Jos hei löydät jostain jotain oikeen fiksua suomen
                    kielistä ohjetta niin vinkkaa mulle privana.
                </p>
            </div>
        </noscript>
        <router-view></router-view>
    </div>

    <div class="container text-light mb-5">
        <p>
            Blizzard Entertainment's&reg; images, text or sound are used under terms described here
            <a href="http://us.blizzard.com/en-us/company/about/legal-faq.html" target="_blank" class="text-light">
                http://us.blizzard.com/en-us/company/about/legal-faq.html
            </a>
            and they are &copy; 2004 Blizzard Entertainment, Inc. All rights reserved. World of Warcraft, Warcraft and
            Blizzard Entertainment are trademarks or registered trademarks of Blizzard Entertainment, Inc. in the U.S.
            and/or other countries.
        </p>
        <p>
            All other content is &copy; {{ date('Y') }} Arctic Circle The World of Warcraft gaming community and or
            Kehet. All rights reserved unless otherwise mentioned.
        </p>
    </div>
</div>

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
