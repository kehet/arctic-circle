@extends('errors::illustrated-layout')

@section('code', '419')
@section('title', __('Sivu on vanhentunut'))

@section('image')
    <div style="background-image: url({{ asset('/svg/403.svg') }});"
         class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __('Sivu on vanhentunut. Päivitä sivu ja yritä uudelleen'))
