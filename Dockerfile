# Set the base image for subsequent instructions
FROM php:7.2

# Update packages
RUN apt-get update

# Install PHP and composer dependencies
RUN apt-get install -qq git curl libmcrypt-dev libjpeg-dev libpng-dev libfreetype6-dev libbz2-dev libxml2-dev unzip

# Clear out the local repository of retrieved package files
RUN apt-get clean

# Install needed extensions
# Here you can install any other extension that you need during the test and deployment process
RUN docker-php-ext-install pdo_mysql mbstring tokenizer xml ctype json bcmath zip pcntl

# Install Composer
RUN curl --silent --show-error https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install SensioLabs Security Checker
RUN curl --silent --show-error https://get.sensiolabs.org/security-checker.phar -o /usr/local/bin/security-checker
RUN chmod 755 /usr/local/bin/security-checker

# Install Laravel Envoy
RUN composer global require "laravel/envoy=~1.0"
