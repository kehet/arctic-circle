<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App{
/**
 * App\Comment
 *
 * @property int $id
 * @property int $commentable_id
 * @property string $commentable_type
 * @property int $user_id
 * @property string $body_html
 * @property string $body_md
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $commentable
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereBodyHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereBodyMd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCommentableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCommentableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Comment whereUserId($value)
 */
	class Comment extends \Eloquent {}
}

namespace App{
/**
 * App\BlogPost
 *
 * @property int $id
 * @property int $created_by
 * @property int|null $updated_by
 * @property string $title
 * @property string $body_html
 * @property string $body_md
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $cover
 * @property string|null $thumbnail
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read \App\User $createdBy
 * @property-read \App\User|null $updatedBy
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereBodyHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereBodyMd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereThumbnail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\BlogPost whereUpdatedBy($value)
 */
	class BlogPost extends \Eloquent {}
}

namespace App{
/**
 * App\Application
 *
 * @property int $id
 * @property int $user_id
 * @property string $body_html
 * @property string $body_md
 * @property string $status
 * @property string|null $message_html
 * @property string|null $message_md
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $character_id
 * @property-read \App\Character $character
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereBodyHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereBodyMd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereMessageHtml($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereMessageMd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Application whereUserId($value)
 */
	class Application extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $battletag
 * @property string $provider_id
 * @property string|null $provider_token
 * @property int $rank
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property int|null $character_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Application[] $applications
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogPost[] $blogPostEdited
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\BlogPost[] $blogPosts
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Character[] $characters
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read mixed $is_admin
 * @property-read mixed $is_member
 * @property-read \App\Character $main
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereBattletag($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCharacterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProviderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereProviderToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRank($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

namespace App {
    /**
     * App\Guild
     *
     * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[]
     *                $notifications
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Guild newModelQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Guild newQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Guild query()
     */
    class Guild extends \Eloquent
    {
    }
}

namespace App {
    /**
     * App\Character
     *
     * @property int $id
     * @property int $user_id
     * @property string $slug
     * @property string $name
     * @property string $realm
     * @property string|null $guildSlug
     * @property string|null $guild
     * @property string|null $guildRealm
     * @property string $level
     * @property string $class
     * @property string $className
     * @property string $race
     * @property string $raceName
     * @property string|null $spec
     * @property string $avatar
     * @property string|null $armory
     * @property string|null $raider_io
     * @property string|null $warcraft_logs
     * @property \Illuminate\Support\Carbon|null $created_at
     * @property \Illuminate\Support\Carbon|null $updated_at
     * @property-read \App\User $user
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character newModelQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character newQuery()
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character query()
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereArmory($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereAvatar($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereClass($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereClassName($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereCreatedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereGuild($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereGuildRealm($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereGuildSlug($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereId($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereLevel($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereName($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereRace($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereRaceName($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereRaiderIo($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereRealm($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereSlug($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereSpec($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereUpdatedAt($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereUserId($value)
     * @method static \Illuminate\Database\Eloquent\Builder|\App\Character whereWarcraftLogs($value)
     */
    class Character extends \Eloquent
    {
    }
}

