<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
//*/

Route::get('posts', 'BlogPostController@index');
Route::get('posts/{blogPost}', 'BlogPostController@show');
Route::get('recruitment_status', 'RecruitmentController@show');
Route::get('rules', 'RulesController@show');
Route::get('reports', 'WarcraftLogsController@latest');
Route::get('users', 'UserController@index');
Route::get('apply_text', 'ApplyTextController@show');

Route::middleware(['auth:api'])->group(function () {
    Route::post('posts/{blogPost}', 'BlogPostController@update');
    Route::delete('posts/{blogPost}', 'BlogPostController@destroy');

    Route::get('/profile', 'UserController@current');
    Route::get('/profile/characters', 'CharacterController@list');
    Route::post('/profile/name', 'CharacterController@setActive');

    Route::get('/applications', 'ApplicationController@index');
    Route::post('/applications', 'ApplicationController@store');
    Route::get('/applications/{application}', 'ApplicationController@show');
    Route::post('/applications/{application}', 'ApplicationController@update');
    Route::post('/applications/{application}/comments', 'ApplicationController@comment');

    Route::post('/posts', 'BlogPostController@store');
    Route::post('/recruitment_status', 'RecruitmentController@store');
    Route::get('/recruitment_status/raw', 'RecruitmentController@raw');
    Route::post('/rules', 'RulesController@store');
    Route::get('/rules/raw', 'RulesController@raw');
    Route::get('apply_text/raw', 'ApplyTextController@raw');
    Route::post('apply_text', 'ApplyTextController@store');
});
