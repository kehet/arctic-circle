<?php


namespace App\Channels;


class DiscordMessage
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $content;

    /**
     * @var null|string
     */
    private $link;

    /**
     * @var null|string
     */
    private $image;
    /**
     * @var bool
     */
    private $imageIsBig;

    /**
     * @param string $title
     * @param string $content
     * @param string|null $link
     * @param string|null $image
     * @param bool $imageIsBig
     */
    public function __construct($title, $content, $link = null, $image = null, $imageIsBig = false)
    {
        $this->title = $title;
        $this->content = $content;
        $this->link = $link;
        $this->image = $image;
        $this->imageIsBig = $imageIsBig;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @return null|string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return bool
     */
    public function isImageIsBig(): bool
    {
        return $this->imageIsBig;
    }


}
