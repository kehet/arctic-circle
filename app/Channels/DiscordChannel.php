<?php

namespace App\Channels;

use GuzzleHttp\Client;
use Illuminate\Notifications\Notification;

class DiscordChannel
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed $notifiable
     * @param  \Illuminate\Notifications\Notification $notification
     *
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        /** @var \App\Channels\DiscordMessage $message */
        $message = $notification->toDiscord($notifiable);

        $embed = [
            'title'       => $message->getTitle(),
            'description' => $message->getContent(),
        ];

        if (!empty($message->getLink())) {
            $embed['url'] = $message->getLink();
        }

        if (!empty($message->getImage())) {
            $embed[$message->isImageIsBig() ? 'image' : 'thumbnail'] = [
                'url' => $message->getImage(),
            ];
        }

        $this->client->post(config('services.discord.webhook'), [
            'json' => [
                'username' => 'AC-Bot',
                'embeds'   => [
                    $embed,
                ],
            ],
        ]);
    }
}
