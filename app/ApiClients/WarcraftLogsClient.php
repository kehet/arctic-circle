<?php


namespace App\ApiClients;


use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class WarcraftLogsClient
{

    /** @var \GuzzleHttp\Client */
    protected $guzzle;

    /** @var string */
    protected $region;

    /** @var string */
    protected $key;

    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
        $this->region = config('services.warcraftlogs.region');
        $this->key = config('services.warcraftlogs.key');
    }

    /**
     * Gets an array of Report objects. Each Report corresponds to a single calendar report for the specified guild.
     *
     * @param string $name
     * @param string $realm
     *
     * @return \Illuminate\Support\Collection
     */
    public function getGuildReports(string $name, string $realm): Collection
    {
        $key = 'warcraftlogs.reports.' . rawurlencode($name) . '.' . rawurlencode($realm);
        $ttl = 60 * 14;

        return Cache::remember($key, $ttl, function () use ($realm, $name) {
            $response = $this->guzzle->get(
                'https://www.warcraftlogs.com:443/v1/reports/guild/' .
                rawurlencode($name) . '/' . rawurlencode($realm) . '/' . $this->region .
                '?start=' . now()->subDays(30)->format('U') . '000' .
                '&api_key=' . $this->key
            );

            return collect(\GuzzleHttp\json_decode((string)$response->getBody()));
        });
    }

    /**
     * Gets arrays of fights and the participants in those fights. Each Fight corresponds to a single pull of a boss.
     *
     * @param string $reportId
     * @param bool $useShortCache
     *
     * @return \Illuminate\Support\Collection
     */
    public function getFights(string $reportId, bool $useShortCache = false): Collection
    {
        $key = 'warcraftlogs.fights.' . rawurlencode($reportId);
        $ttl = ($useShortCache ? 60 * 14 : (60 * 60 * 24 * 30));

        return Cache::remember($key, $ttl, function () use ($reportId) {
            $response = $this->guzzle->get(
                'https://www.warcraftlogs.com:443/v1/report/fights/' .
                rawurlencode($reportId) .
                '?api_key=' . $this->key
            );

            return collect(\GuzzleHttp\json_decode((string)$response->getBody())->fights);
        });
    }

    /**
     * Gets an array of Zone objects. Each zone corresponds to a raid/dungeon instance in the game and has its own set
     * of encounters.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getZones(): Collection
    {
        $key = 'warcraftlogs.zones';
        $ttl = 60 * 60 * 24 * 30;

        return Cache::remember($key, $ttl, function () {
            $response = $this->guzzle->get(
                'https://www.warcraftlogs.com:443/v1/zones' .
                '?api_key=' . $this->key
            );

            return collect(\GuzzleHttp\json_decode((string)$response->getBody()));
        });
    }

}
