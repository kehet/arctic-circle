<?php


namespace App\ApiClients;

use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use stdClass;

class BattleNetClient
{
    /** @var \GuzzleHttp\Client */
    protected $guzzle;

    /** @var string */
    protected $region;

    /** @var string */
    protected $key;

    public function __construct(Client $guzzle)
    {
        $this->guzzle = $guzzle;
        $this->region = config('services.battlenet.region');
        $this->key = config('services.battlenet.client_id');
    }

    /**
     *
     */
    public function fetchClientCredentialsAccessToken()
    {
        $key = 'api.oauth.clientcredentials.token';

        if (Cache::has($key)) {
            return Cache::get($key);
        }

        $response = $this->guzzle->post(
            'https://' . $this->region . '.battle.net/oauth/token',
            [
                'auth'        => [
                    config('services.battlenet.client_id'),
                    config('services.battlenet.client_secret'),
                ],
                'form_params' => [
                    'grant_type' => 'client_credentials',
                ],
            ]
        );

        $response = \GuzzleHttp\json_decode((string)$response->getBody());

        Cache::put($key, $response, $response->expires_in - 1);

        return $response;
    }

    /**
     * @param string $name
     *
     * @param string $realm
     *
     * @return \stdClass
     */
    public function guildInfo(string $name, string $realm): stdClass
    {
        $key = 'api.guild.' . rawurlencode($name) . '.' . rawurlencode($realm);
        $ttl = 60 * 60;
        $accessToken = $this->fetchClientCredentialsAccessToken()->access_token;

        return Cache::remember($key, $ttl, function () use ($realm, $name, $accessToken) {
            $response = $this->guzzle->get(
                'https://' . $this->region . '.api.blizzard.com/wow/guild/' .
                rawurlencode($realm) . '/' . rawurlencode($name) .
                '?region=' . $this->region .
                '&locale=en_GB' .
                '&access_token=' . $accessToken
            );

            return \GuzzleHttp\json_decode((string)$response->getBody());
        });
    }

    /**
     * @param string $name
     *
     * @param string $realm
     *
     * @return Collection
     */
    public function guildMembers(string $name, string $realm): Collection
    {
        $key = 'api.guild.' . rawurlencode($name) . '.' . rawurlencode($realm) . '.members';
        $ttl = 60 * 60;
        $accessToken = $this->fetchClientCredentialsAccessToken()->access_token;

        return Cache::remember($key, $ttl, function () use ($realm, $name, $accessToken) {
            $response = $this->guzzle->get(
                'https://' . $this->region . '.api.blizzard.com/wow/guild/' .
                rawurlencode($realm) . '/' . rawurlencode($name) .
                '?fields=members' .
                '&region=' . $this->region .
                '&locale=en_GB' .
                '&access_token=' . $accessToken
            );

            $json = \GuzzleHttp\json_decode((string)$response->getBody());
            return collect($json->members);
        });
    }

    /**
     * @param \App\User $user
     *
     * @return \Illuminate\Support\Collection
     */
    public function currentCharacters(User $user): Collection
    {
        $key = 'api.characters.' . $user->provider_id;
        $ttl = 60 * 60;
        $accessToken = $user->provider_token;

        return Cache::remember($key, $ttl, function () use ($accessToken) {
            $response = $this->guzzle->get(
                'https://' . $this->region . '.api.blizzard.com/wow/user/characters' .
                '?access_token=' . $accessToken
            );

            $json = \GuzzleHttp\json_decode((string)$response->getBody());

            return collect($json->characters);
        });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function dataRaces(): Collection
    {
        return $this->dataRacesIndex()
            ->map(function ($race) {
                return $this->dataRaceHref($race->key->href);
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function dataRacesIndex(): Collection
    {
        $key = 'api.data.races.index';
        $ttl = 60 * 60 * 24 * 30;
        $accessToken = $this->fetchClientCredentialsAccessToken()->access_token;

        return Cache::remember($key, $ttl, function () use ($accessToken) {
            $response = $this->guzzle->get(
                'https://' . $this->region . '.api.blizzard.com/data/wow/race/index' .
                '?locale=en_us' .
                '&region=' . $this->region .
                '&namespace=static-' . $this->region .
                '&access_token=' . $accessToken
            );

            $json = \GuzzleHttp\json_decode((string)$response->getBody());

            return collect($json->races);
        });
    }

    /**
     * @param string $href
     *
     * @return \stdClass
     */
    public function dataRaceHref($href): stdClass
    {
        $key = 'api.data.races.' . md5($href);
        $ttl = 60 * 60 * 24 * 30;
        $accessToken = $this->fetchClientCredentialsAccessToken()->access_token;

        return Cache::remember($key, $ttl, function () use ($href, $accessToken) {
            $response = $this->guzzle->get(
                $href .
                '&locale=en_us' .
                '&region=' . $this->region .
                '&access_token=' . $accessToken
            );

            $json = \GuzzleHttp\json_decode((string)$response->getBody());

            return (object)$json;
        });
    }

    /**
     * @param string $id
     *
     * @return \stdClass
     */
    public function dataRaceId($id): stdClass
    {
        $key = 'api.data.races.' . $id;
        $ttl = 60 * 60 * 24 * 30;
        $accessToken = $this->fetchClientCredentialsAccessToken()->access_token;

        return Cache::remember($key, $ttl, function () use ($id, $accessToken) {
            $response = $this->guzzle->get(
                'https://' . $this->region . '.api.blizzard.com/data/wow/race/' . $id .
                '?locale=en_us' .
                '&region=' . $this->region .
                '&namespace=static-' . $this->region .
                '&access_token=' . $accessToken
            );

            $json = \GuzzleHttp\json_decode((string)$response->getBody());

            return (object)$json;
        });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function dataClasses(): Collection
    {
        return $this->dataClassesIndex()
            ->map(function ($race) {
                return $this->dataClass($race->key->href);
            });
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function dataClassesIndex(): Collection
    {
        $key = 'api.data.classes.index';
        $ttl = 60 * 60 * 24 * 30;
        $accessToken = $this->fetchClientCredentialsAccessToken()->access_token;

        return Cache::remember($key, $ttl, function () use ($accessToken) {
            $response = $this->guzzle->get(
                'https://' . $this->region . '.api.blizzard.com/data/wow/playable-class/index' .
                '?locale=en_us' .
                '&region=' . $this->region .
                '&namespace=static-' . $this->region .
                '&access_token=' . $accessToken
            );

            $json = \GuzzleHttp\json_decode((string)$response->getBody());

            return collect($json->classes);
        });
    }

    /**
     * @param string $href
     *
     * @return \stdClass
     */
    public function dataClass($href): stdClass
    {
        $key = 'api.data.classes.' . md5($href);
        $ttl = 60 * 60 * 24 * 30;
        $accessToken = $this->fetchClientCredentialsAccessToken()->access_token;

        return Cache::remember($key, $ttl, function () use ($href, $accessToken) {
            $response = $this->guzzle->get(
                $href .
                '&locale=en_us' .
                '&region=' . $this->region .
                '&access_token=' . $accessToken
            );

            $json = \GuzzleHttp\json_decode((string)$response->getBody());

            return (object)$json;
        });
    }
}
