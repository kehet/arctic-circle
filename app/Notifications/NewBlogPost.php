<?php

namespace App\Notifications;

use App\BlogPost;
use App\Channels\DiscordChannel;
use App\Channels\DiscordMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewBlogPost extends Notification
{
    use Queueable;

    /**
     * @var \App\BlogPost
     */
    private $blogPost;

    /**
     * Create a new notification instance.
     *
     * @param \App\BlogPost $blogPost
     */
    public function __construct(BlogPost $blogPost)
    {
        $this->blogPost = $blogPost;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [DiscordChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Arctic-Circle Uusi uutinen')
            ->greeting('Uusi uutinen!')
            ->action($this->blogPost->title, url('/') . '#/');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title'   => 'Uusi uutinen',
            'subject' => $this->blogPost->title,
            'url'     => url('/') . '/#/',
        ];
    }

    public function toDiscord($notifiable)
    {
        return new DiscordMessage(
            'Uusi uutinen!',
            $this->blogPost->title,
            url('/') . '/#/',
            asset('storage/' . $this->blogPost->thumbnail),
            true
        );
    }
}
