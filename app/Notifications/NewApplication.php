<?php

namespace App\Notifications;

use App\Application;
use App\Channels\DiscordChannel;
use App\Channels\DiscordMessage;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewApplication extends Notification
{
    use Queueable;

    /**
     * @var \App\Application
     */
    private $application;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function via($notifiable)
    {
        return [DiscordChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Arctic-Circle Uusi hakemus on saapunut')
            ->greeting('Uusi hakemus on saapunut!')
            ->line('Hakija: ' . $this->application->character->name . ' (' . $this->application->character->className . ')')
            ->action('Hakemukseen', url('/') . '#/applications/' . $this->application->id);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     *
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title'       => 'Uusi hakemus on saapunut',
            'url'         => url('/') . '#/applications/' . $this->application->id,
            'application' => $this->application->id,
            'character'   => [
                'name'  => $this->application->character->name,
                'class' => $this->application->character->className,
            ],
        ];
    }

    public function toDiscord($notifiable)
    {
        $author = $this->application->character->name . '-' . $this->application->character->realm . ' (' . $this->application->character->className . ')';

        return new DiscordMessage(
            'Uusi hakemus on saapunut!',
            $author,
            url('/') . '/#/applications/' . $this->application->id,
            $this->application->character->avatar
        );
    }
}
