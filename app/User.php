<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use OwenIt\Auditing\Contracts\Auditable;

class User extends Authenticatable implements Auditable
{
    use HasApiTokens, Notifiable, \OwenIt\Auditing\Auditable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'rank',
    ];

    protected $hidden = [
        'battletag',
        'provider_id',
        'provider_token',
        'remember_token',
    ];

    protected $casts = [
        'character' => 'object',
    ];

    /**
     * Attributes to exclude from the Audit.
     *
     * @var array
     */
    protected $auditExclude = [
        'provider_token',
        'remember_token',
    ];

    public function getIsAdminAttribute(): bool
    {
        return ($this->rank <= 1);
    }

    public function getIsMemberAttribute(): bool
    {
        return ($this->rank < 99);
    }

    public function blogPosts()
    {
        return $this->hasMany(BlogPost::class, 'created_at');
    }

    public function blogPostEdited()
    {
        return $this->hasMany(BlogPost::class, 'updated_at');
    }

    public function applications()
    {
        return $this->hasMany(Application::class);
    }

    public function main()
    {
        return $this->hasOne(Character::class, 'id', 'character_id');
    }

    public function characters()
    {
        return $this->hasMany(Character::class);
    }
}
