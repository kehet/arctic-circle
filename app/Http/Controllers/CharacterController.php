<?php

namespace App\Http\Controllers;

use App\ApiClients\BattleNetClient;
use App\Character;
use Illuminate\Http\Request;

class CharacterController extends Controller
{

    /**
     * @var \App\ApiClients\BattleNetClient
     */
    private $client;

    public function __construct(BattleNetClient $client)
    {
        $this->client = $client;
    }

    public function list()
    {
        $characters = \Auth::user()->characters
            ->map(function ($character) {
                /** @var Character $character */

                return $character->only([
                    'slug',
                    'name',
                    'realm',
                    'guildSlug',
                    'guild',
                    'guildRealm',
                    'level',
                    'class',
                    'className',
                    'race',
                    'raceName',
                    'spec',
                    'avatar',
                    'armory',
                    'raider_io',
                    'warcraft_logs',
                ]);
            })
            ->groupBy('realm')
            ->map(function ($characters, $key) {
                $characters = $characters->toArray();

                usort($characters, function ($a, $b) {
                    if ($a['level'] < $b['level']) return 1;
                    if ($a['level'] > $b['level']) return -1;

                    if ($a['name'] > $b['name']) return 1;
                    if ($a['name'] < $b['name']) return -1;

                    return 0;
                });

                return (object)[
                    'name'       => $key,
                    'slug'       => str_slug($key),
                    'characters' => $characters,
                ];
            })
            ->sortBy('name')
            ->values();

        return jsend_success($characters);
    }

    public function setActive(Request $request)
    {
        $request->validate([
            'slug' => 'required',
        ]);

        $characters = \Auth::user()->characters;

        $foundCharacter = null;
        foreach ($characters as $character) {
            if ($character->slug == $request->get('slug')) {
                $foundCharacter = $character;
                break;
            }
        }

        if ($foundCharacter == null) {
            abort(404, 'Character not found');
        }

        $user = \Auth::user();
        $user->name = $foundCharacter->name;
        $user->character_id = $foundCharacter->id;
        $user->save();

        $name = 'Arctic Circle';
        $realm = 'Darkspear';

        $members = $this->client->guildMembers($name, $realm)
            ->map(function ($member) {
                return (object)[
                    'name'  => $member->character->name,
                    'realm' => $member->character->realm,
                    'slug'  => str_slug($member->character->name . '-' . $member->character->realm),
                    'rank'  => $member->rank,
                ];
            });

        $isMember = false;
        foreach ($members as $member) {
            if ($foundCharacter->slug == $member->slug) {
                $isMember = true;

                if ($user->rank != $member->rank) {
                    $user->update([
                        'rank' => $member->rank,
                    ]);
                }

                break;
            }
        }

        if (!$isMember && $user->rank != 99) {
            $user->update([
                'rank' => 99,
            ]);
        }

        return jsend_success([]);
    }

}
