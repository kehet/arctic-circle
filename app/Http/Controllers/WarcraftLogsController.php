<?php

namespace App\Http\Controllers;

use App\Jobs\FetchLatestWarcraftLogs;
use Illuminate\Support\Facades\Cache;

class WarcraftLogsController extends Controller
{

    public function latest()
    {
        $reports = [];

        if (Cache::has('warcraftlogs-parsed')) {
            $reports = Cache::get('warcraftlogs-parsed');
        } else {
            FetchLatestWarcraftLogs::dispatch();
        }

        return jsend_success(compact('reports'));
    }

}
