<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;


class ApplyTextController extends Controller
{
    public function show()
    {
        if (!Storage::disk()->exists('apply_promo.html')) {
            Storage::disk()->write('apply_promo.html', '<p><em>Not set</em></p>');
        }

        if (!Storage::disk()->exists('apply_help.html')) {
            Storage::disk()->write('apply_help.html', '<p><em>Not set</em></p>');
        }

        return jsend_success([
            'promoText' => Storage::disk()->get('apply_promo.html'),
            'helpText' => Storage::disk()->get('apply_help.html'),
        ]);
    }

    public function raw()
    {
        if (!Storage::disk()->exists('apply_promo.md')) {
            Storage::disk()->write('apply_promo.md', '**Not set**');
        }
        if (!Storage::disk()->exists('apply_help.md')) {
            Storage::disk()->write('apply_help.md', '**Not set**');
        }

        return jsend_success([
            'promoText' => Storage::disk()->get('apply_promo.md'),
            'helpText' => Storage::disk()->get('apply_help.md'),
        ]);
    }

    public function store(Request $request)
    {
        abort_if(Auth::guest(), 401, 'Unauthenticated.');
        abort_unless(Auth::user()->is_admin, 401, 'This action is unauthorized.');

        $request->validate([
            'promoText' => 'required|max:65535',
            'helpText' => 'required|max:65535',
        ]);

        $oldPromo = Storage::disk()->get('apply_promo.md');
        $oldHelp = Storage::disk()->get('apply_help.md');

        Storage::put('apply_promo.md', $request->get('promoText'));
        Storage::put('apply_promo.html', markdown($request->get('promoText')));

        Storage::put('apply_help.md', $request->get('helpText'));
        Storage::put('apply_help.html', markdown($request->get('helpText')));

        Log::info('Apply promo message changed', [
            'user'      => Auth::user()->toArray(),
            'ip'        => $request->ip(),
            'agent'     => $request->header('User-Agent'),
            'old_value' => $oldPromo,
            'new_value' => $request->get('promoText'),
        ]);

        Log::info('Apply help message changed', [
            'user'      => Auth::user()->toArray(),
            'ip'        => $request->ip(),
            'agent'     => $request->header('User-Agent'),
            'old_value' => $oldHelp,
            'new_value' => $request->get('helpText'),
        ]);

        return jsend_success([]);
    }
}
