<?php

namespace App\Http\Controllers;

use App\Application;
use App\Comment;
use App\Guild;
use App\Notifications\NewApplication;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicationController extends Controller
{

    public function index()
    {
        $this->authorize('index', Application::class);

        $applications = Application::with('user')->orderBy('created_at', 'desc')->get();

        if ($applications->isEmpty()) {
            return jsend_success();
        }

        return jsend_success($applications->map(function ($application) {
            return [
                'id'     => $application->id,
                'status' => $application->status,
                'user'       => [
                    'name'      => $application->user->name,
                    'character' => $application->character->only([
                        'slug',
                        'name',
                        'realm',
                        'guildSlug',
                        'guild',
                        'guildRealm',
                        'level',
                        'class',
                        'className',
                        'race',
                        'raceName',
                        'spec',
                        'avatar',
                        'armory',
                        'raider_io',
                        'warcraft_logs',
                    ]),
                ],
                'created_at' => $application->created_at->format(DateTime::ISO8601),
                'updated_at' => $application->created_at->format(DateTime::ISO8601),
            ];
        })->toArray());
    }

    public function show(Application $application)
    {
        $this->authorize('view', $application);

        $application->load(['user', 'comments']);

        return jsend_success([
            'id'           => $application->id,
            'status'       => $application->status,
            'body_html'    => $application->body_html,
            'body_md'      => $application->body_md,
            'message_html' => $application->message_html ?? null,
            'message_md'   => $application->message_md ?? null,
            'user'         => [
                'name'      => $application->user->name,
                'character' => $application->character->only([
                    'slug',
                    'name',
                    'realm',
                    'guildSlug',
                    'guild',
                    'guildRealm',
                    'level',
                    'class',
                    'className',
                    'race',
                    'raceName',
                    'spec',
                    'avatar',
                    'armory',
                    'raider_io',
                    'warcraft_logs',
                ]),
            ],
            'comments'     => $application->comments->map(function ($comment) {
                return [
                    'body_html'  => $comment->body_html,
                    'body_md'    => $comment->body_md,
                    'created_at' => $comment->created_at->format(DateTime::ISO8601),
                    'user'       => [
                        'name'      => $comment->user->name,
                        'character' => $comment->user->main->only([
                            'slug',
                            'name',
                            'realm',
                            'guildSlug',
                            'guild',
                            'guildRealm',
                            'level',
                            'class',
                            'className',
                            'race',
                            'raceName',
                            'spec',
                            'avatar',
                            'armory',
                            'raider_io',
                            'warcraft_logs',
                        ]),
                    ],
                ];
            }),
            'created_at'   => $application->created_at->format(DateTime::ISO8601),
            'updated_at'   => $application->created_at->format(DateTime::ISO8601),
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('create', Application::class);

        $request->validate([
            'body' => 'required|min:500',
        ]);

        $application = new Application();
        $application->user_id = Auth::id();
        $application->body_html = $request->input('body');
        $application->body_md = markdown($request->input('body'));
        $application->character_id = optional(Auth::user())->character_id;
        $application->save();

        if (!\App::environment('local')) {
            with(new Guild())->notify(new NewApplication($application));
        }

        return jsend_success([]);
    }

    public function update(Request $request, Application $application)
    {
        $this->authorize('update', $application);

        $request->validate([
            'message' => '',
            'status'  => '',
        ]);

        $application->message_html = $request->input('message');
        $application->message_md = markdown($request->input('message'));
        $application->status = $request->input('status');
        $application->save();

        $application->load(['user', 'comments' => function ($query) {
            $query->orderBy('created_at', 'asc');
        }]);

        return jsend_success([
            'id'           => $application->id,
            'status'       => $application->status,
            'body_html'    => $application->body_html,
            'body_md'      => $application->body_md,
            'message_html' => $application->message_html ?? null,
            'message_md'   => $application->message_md ?? null,
            'user'         => [
                'name'      => $application->user->name,
                'character' => $application->character->only([
                    'slug',
                    'name',
                    'realm',
                    'guildSlug',
                    'guild',
                    'guildRealm',
                    'level',
                    'class',
                    'className',
                    'race',
                    'raceName',
                    'spec',
                    'avatar',
                    'armory',
                    'raider_io',
                    'warcraft_logs',
                ]),
            ],
            'comments'   => $application->comments->map(function ($comment) {
                return [
                    'body_html'  => $comment->body_html,
                    'body_md'    => $comment->body_md,
                    'created_at' => $comment->created_at->format(DateTime::ISO8601),
                    'user'       => [
                        'name'      => $comment->user->name,
                        'character' => $comment->user->main->only([
                            'slug',
                            'name',
                            'realm',
                            'guildSlug',
                            'guild',
                            'guildRealm',
                            'level',
                            'class',
                            'className',
                            'race',
                            'raceName',
                            'spec',
                            'avatar',
                            'armory',
                            'raider_io',
                            'warcraft_logs',
                        ]),
                    ],
                ];
            }),
            'created_at' => $application->created_at->format(DateTime::ISO8601),
            'updated_at' => $application->created_at->format(DateTime::ISO8601),
        ]);
    }

    public function comment(Request $request, Application $application)
    {
        $this->authorize('comment', $application);

        $request->validate([
            'message' => '',
        ]);

        $comment = new Comment();
        $comment->body_html = markdown($request->input('message'));
        $comment->body_md = $request->input('message');
        $comment->user_id = Auth::id();

        $application->comments()->save($comment);

        $application->load(['user', 'comments' => function ($query) {
            $query->orderBy('created_at', 'asc');
        }]);

        return jsend_success([
            'id'           => $application->id,
            'status'       => $application->status,
            'body_html'    => $application->body_html,
            'body_md'      => $application->body_md,
            'message_html' => $application->message_html ?? null,
            'message_md'   => $application->message_md ?? null,
            'user'         => [
                'name'      => $application->user->name,
                'character' => $application->character->only([
                    'slug',
                    'name',
                    'realm',
                    'guildSlug',
                    'guild',
                    'guildRealm',
                    'level',
                    'class',
                    'className',
                    'race',
                    'raceName',
                    'spec',
                    'avatar',
                    'armory',
                    'raider_io',
                    'warcraft_logs',
                ]),
            ],
            'comments'   => $application->comments->map(function ($comment) {
                return [
                    'body_html'  => $comment->body_html,
                    'body_md'    => $comment->body_md,
                    'created_at' => $comment->created_at->format(DateTime::ISO8601),
                    'user'       => [
                        'name'      => $comment->user->name,
                        'character' => $comment->user->main->only([
                            'slug',
                            'name',
                            'realm',
                            'guildSlug',
                            'guild',
                            'guildRealm',
                            'level',
                            'class',
                            'className',
                            'race',
                            'raceName',
                            'spec',
                            'avatar',
                            'armory',
                            'raider_io',
                            'warcraft_logs',
                        ]),
                    ],
                ];
            }),
            'created_at' => $application->created_at->format(DateTime::ISO8601),
            'updated_at' => $application->created_at->format(DateTime::ISO8601),
        ]);
    }
}
