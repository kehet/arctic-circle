<?php

namespace App\Http\Controllers;


use App\User;
use DateTime;

class UserController extends Controller
{

    public function current()
    {
        $user = \Auth::user();

        if (\Auth::guest()) {
            return jsend_success(compact('user'));
        }

        $application = $user->applications->first();

        $user = [
            'name'      => $user->name,
            'character' => $user->main->only([
                'slug',
                'name',
                'realm',
                'guildSlug',
                'guild',
                'guildRealm',
                'level',
                'class',
                'className',
                'race',
                'raceName',
                'spec',
                'avatar',
                'armory',
                'raider_io',
                'warcraft_logs',
            ]),
            'rank'      => $user->rank,
            'is_member' => $user->is_member,
            'is_admin'  => $user->is_admin,
        ];

        return jsend_success(compact('user'));
    }

    public function index()
    {
        $users = User::with('main')
            ->orderByRaw('rank asc, name asc')
            ->get()
            ->map(function($user) {
                return (object)[
                    'name'       => $user->name,
                    'created_at' => $user->created_at->format(DateTime::ISO8601),
                    'character'  => empty($user->main) ? null : $user->main->only([
                        'slug',
                        'name',
                        'realm',
                        'guildSlug',
                        'guild',
                        'guildRealm',
                        'level',
                        'class',
                        'className',
                        'race',
                        'raceName',
                        'spec',
                        'avatar',
                        'armory',
                        'raider_io',
                        'warcraft_logs',
                    ]),
                    'rank'      => $user->rank,
                    'is_member' => $user->is_member,
                    'is_admin'  => $user->is_admin,
                ];
            })
            ->filter(function($user) {
                return $user->is_member;
            });

        return jsend_success(compact('users'));
    }

}
