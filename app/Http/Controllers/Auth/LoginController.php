<?php

namespace App\Http\Controllers\Auth;

use App;
use App\Http\Controllers\Controller;
use App\Jobs\FetchUserCharacters;
use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('battlenet')->scopes(['wow.profile'])->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {
        try {
            $socialUser = Socialite::with('battlenet')->user();
        } catch (ClientException $e) {
            return abort(500, App::environment('production') ? 'Kirjautuminen epäonnistui :(' : $e->getMessage());
        }

        $user = User::where('provider_id', $socialUser->id)->first();

        if ($user == null) {
            $newUser = new User();

            $newUser->name = explode('#', $socialUser->getNickname())[0];
            $newUser->battletag = $socialUser->getNickname();
            $newUser->provider_id = $socialUser->getId();
            $newUser->provider_token = $socialUser->token;

            $newUser->save();

            $user = $newUser;
            FetchUserCharacters::dispatch($user)->onQueue('high');

            Log::info('New user', [
                'user'  => $user->toArray(),
                'ip'    => Request::ip(),
                'agent' => Request::header('User-Agent'),
            ]);
        } else {
            $user->provider_token = $socialUser->token;
            $user->save();
            FetchUserCharacters::dispatch($user);

            Log::info('User logged in', [
                'user'  => $user->toArray(),
                'ip'    => Request::ip(),
                'agent' => Request::header('User-Agent'),
            ]);
        }

        Auth::login($user);

        return redirect('/#/');
    }

    public function logout()
    {
        Auth::logout();

        return redirect('/#/');
    }
}
