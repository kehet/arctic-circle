<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class RecruitmentController extends Controller
{
    public function show()
    {
        if (!Storage::disk()->exists('recruitment.html')) {
            Storage::disk()->write('recruitment.html', '<p><em>Status not set</em></p>');
        }

        return jsend_success(['status' => Storage::disk()->get('recruitment.html')]);
    }

    public function raw()
    {
        if (!Storage::disk()->exists('recruitment.md')) {
            Storage::disk()->write('recruitment.md', '**Status not set**');
        }

        return jsend_success(['status' => Storage::disk()->get('recruitment.md')]);
    }

    public function store(Request $request)
    {
        abort_if(Auth::guest(), 401, 'Unauthenticated.');
        abort_unless(Auth::user()->is_admin, 401, 'This action is unauthorized.');

        $request->validate([
            'body' => 'required|max:1024',
        ]);

        $oldValue = Storage::disk()->get('recruitment.md');

        Storage::put('recruitment.md', $request->get('body'));
        Storage::put('recruitment.html', markdown($request->get('body')));

        Log::info('Recruitment message changed', [
            'user'      => Auth::user()->toArray(),
            'ip'        => $request->ip(),
            'agent'     => $request->header('User-Agent'),
            'old_value' => $oldValue,
            'new_value' => $request->get('body'),
        ]);

        return jsend_success([]);
    }
}
