<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Auth;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class RulesController extends Controller
{
    public function show()
    {
        if (!Storage::disk()->exists('rules.json')) {
            Storage::disk()->write('rules.json',
                \GuzzleHttp\json_encode(['updated_by' => 1, 'updated_at' => now()->format(DateTime::ISO8601),]));
        }

        if (!Storage::disk()->exists('rules.html')) {
            Storage::disk()->write('rules.html', '<ol><li>Be Excellent to Each Other</li></ol>');
        }

        $meta = \GuzzleHttp\json_decode(Storage::disk()->get('rules.json'));

        $meta->updated_by = User::find($meta->updated_by);

        return jsend_success([
            'body'       => Storage::disk()->get('rules.html'),
            'updated_at' => $meta->updated_at,
            'updated_by' => [
                'name'      => $meta->updated_by->name,
                'character' => optional($meta->updated_by->main)->only([
                    'name',
                    'realm',
                    'class',
                ]),
            ],
        ]);
    }

    public function raw()
    {
        if (!Storage::disk()->exists('rules.md')) {
            Storage::disk()->write('rules.md', '1. Be Excellent to Each Other');
        }

        return jsend_success(['status' => Storage::disk()->get('rules.md')]);
    }

    public function store(Request $request)
    {
        abort_if(Auth::guest(), 401, 'Unauthenticated.');
        abort_unless(Auth::user()->is_admin, 401, 'This action is unauthorized.');

        $request->validate([
            'body' => 'required|max:65535',
        ]);

        $oldValue = Storage::disk()->get('rules.md');

        Storage::put('rules.json', \GuzzleHttp\json_encode([
            'updated_by' => Auth::id(),
            'updated_at' => now()->format(DateTime::ISO8601),
        ]));
        Storage::put('rules.md', $request->get('body'));
        Storage::put('rules.html', markdown($request->get('body')));

        Log::info('Rules changed', [
            'user'      => Auth::user()->toArray(),
            'ip'        => $request->ip(),
            'agent'     => $request->header('User-Agent'),
            'old_value' => $oldValue,
            'new_value' => $request->get('body'),
        ]);

        return jsend_success([]);
    }
}
