<?php

namespace App\Http\Controllers;

use App\BlogPost;
use App\Guild;
use App\Notifications\NewBlogPost;
use DateTime;
use Illuminate\Http\Request;

class BlogPostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $postsPerPage = $request->get('count', 10);
        $page = $request->get('page', 0);

        abort_if(($postsPerPage < 0 || $postsPerPage > 25), 422, 'count too large or too small');
        abort_if(($page < 0), 422, 'page cant be negative');

        $posts = BlogPost::with(['createdBy', 'updatedBy'])
            ->orderBy('created_at', 'desc')
            ->skip((int)($page * $postsPerPage))
            ->take($postsPerPage)
            ->get();

        return jsend_success($posts->map(function ($post) {
            return [
                'id'         => $post->id,
                'title'      => $post->title,
                'body_html'  => $post->body_html,
                'body_md'    => $post->body_md,
                'cover'      => asset('storage/' . $post->cover),
                'thumbnail'  => asset('storage/' . $post->thumbnail),
                'created_at' => $post->created_at->format(DateTime::ISO8601),
                'updated_at' => $post->updated_at->format(DateTime::ISO8601),
                'created_by' => [
                    'name'      => optional($post->createdBy)->name,
                    'character' => optional(optional($post->createdBy)->main)->only([
                        'name',
                        'realm',
                        'class',
                    ]),
                ],
                'updated_by' => !empty($post->updatedBy) ? [
                    'name'      => optional($post->updatedBy)->name,
                    'character' => optional(optional($post->updatedBy)->main)->only([
                        'name',
                        'realm',
                        'class',
                    ]),
                ] : null,
            ];
        })->toArray());
    }

    /**
     * Display a listing of the resource.
     *
     * @param \App\BlogPost $blogPost
     *
     * @return \Illuminate\Http\Response
     */
    public function show(BlogPost $blogPost)
    {
        return jsend_success([
            'id'         => $blogPost->id,
            'title'      => $blogPost->title,
            'body_html'  => $blogPost->body_html,
            'body_md'    => $blogPost->body_md,
            'cover'      => asset('storage/' . $blogPost->cover),
            'thumbnail'  => asset('storage/' . $blogPost->thumbnail),
            'created_at' => $blogPost->created_at->format(DateTime::ISO8601),
            'updated_at' => $blogPost->updated_at->format(DateTime::ISO8601),
            'created_by' => [
                'name'      => optional($blogPost->createdBy)->name,
                'character' => optional(optional($blogPost->createdBy)->main)->only([
                    'name',
                    'realm',
                    'class',
                ]),
            ],
            'updated_by' => !empty($blogPost->updatedBy) ? [
                'name'      => optional($blogPost->updatedBy)->name,
                'character' => optional(optional($blogPost->updatedBy)->main)->only([
                    'name',
                    'realm',
                    'class',
                ]),
            ] : null,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('create', BlogPost::class);

        $request->validate([
            'title'             => 'required|max:255',
            'body'              => 'required|max:50000',
            'send_notification' => 'required|in:true,false',
            'file'              => 'file|mimes:jpeg,jpg,png',
        ]);

        $blogPost = new BlogPost();
        $blogPost->title = $request->get('title');
        $blogPost->body_html = markdown($request->get('body'));
        $blogPost->body_md = $request->get('body');
        $blogPost->created_by = \Auth::id();

        if ($request->hasFile('file')) {
            $blogPost->uploadImage($request->file('file'), 'cover');
            $blogPost->uploadImage($request->file('file'), 'thumbnail');
        }

        $blogPost->save();

        if ((bool)$request->input('send_notification') && !\App::environment('local')) {
            with(new Guild())->notify(new NewBlogPost($blogPost));
        }

        return jsend_success([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\BlogPost $blogPost
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \QCod\ImageUp\Exceptions\InvalidUploadFieldException
     */
    public function update(BlogPost $blogPost, Request $request)
    {
        $this->authorize('update', $blogPost);

        if ($request->has('file')) {
            $file = $request->file('file');

            if ($file == null) {
                $request->request->remove('file');
            }
        }

        $request->validate([
            'title'             => 'required|max:255',
            'body'              => 'required|max:50000',
            'send_notification' => 'required|in:true,false',
            'file'              => 'sometimes|image|mimes:jpeg,jpg,png',
        ]);

        $blogPost->title = $request->get('title');
        $blogPost->body_html = markdown($request->get('body'));
        $blogPost->body_md = $request->get('body');
        $blogPost->updated_by = \Auth::id();

        if ($request->hasFile('file')) {
            $blogPost->uploadImage($request->file('file'), 'cover');
            $blogPost->uploadImage($request->file('file'), 'thumbnail');
        }

        $blogPost->save();

        if ((bool)$request->input('send_notification') && !\App::environment('local')) {
            with(new Guild())->notify(new NewBlogPost($blogPost));
        }

        return jsend_success([]);
    }

    /**
     * @param \App\BlogPost $blogPost
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(BlogPost $blogPost)
    {
        $this->authorize('destroy', $blogPost);

        $blogPost->delete();

        return jsend_success([]);
    }

}
