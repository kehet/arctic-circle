<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HtmlController extends Controller
{

    public function index()
    {
        $user = Auth::user();

        if ($user != null) {
            $application = $user->applications->first();
            $character = $user->main;

            if (!empty($character)) {
                $character = $character->only([
                    'slug',
                    'name',
                    'realm',
                    'guildSlug',
                    'guild',
                    'guildRealm',
                    'level',
                    'class',
                    'className',
                    'race',
                    'raceName',
                    'spec',
                    'avatar',
                    'armory',
                ]);
            }

            $user = [
                'name'        => $user->name,
                'character'   => $character,
                'rank'        => $user->rank,
                'is_member'   => $user->is_member,
                'is_admin'    => $user->is_admin,
                'application' => $application !== null ? [
                    'status'       => $application->status,
                    'message_html' => $application->message_html,
                    'message_md'   => $application->message_md,
                ] : null,
            ];
        }

        return view('welcome', compact('user'));
    }

}
