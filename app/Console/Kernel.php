<?php

namespace App\Console;

use App\Jobs\FetchLatestWarcraftLogs;
use App\Jobs\UpdateUserRankJob;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->job(new UpdateUserRankJob())->everyThirtyMinutes();

        // during raid times update every 10 minutes, otherwise hourly
        $schedule->job(new FetchLatestWarcraftLogs())->hourly()->mondays()->tuesdays()->fridays()->saturdays();
        $schedule->job(new FetchLatestWarcraftLogs())->hourly()->wednesdays()->thursdays()->sundays()->unlessBetween('20:00', '23:00');
        $schedule->job(new FetchLatestWarcraftLogs())->everyFifteenMinutes()->wednesdays()->thursdays()->sundays()->between('20:00', '23:00');

        $schedule->command('horizon:snapshot')->everyFiveMinutes();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
