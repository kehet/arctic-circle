<?php

namespace App\Console\Commands;

use App\Jobs\FetchLatestWarcraftLogs;
use App\Jobs\UpdateUserRankJob;
use Illuminate\Console\Command;

class CacheWarmUp extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cache:warm-up';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Populates the cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        FetchLatestWarcraftLogs::dispatch()->onQueue('high');
        UpdateUserRankJob::dispatch()->onQueue('high');
    }
}
