<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use QCod\ImageUp\HasImageUploads;

class BlogPost extends Model implements Auditable
{

    use HasImageUploads, \OwenIt\Auditing\Auditable;

    protected $table = 'blog_posts';

    protected $fillable = [
        'title',
        'body',
    ];

    protected $appends = [
        'created_by',
        'updated_by',
    ];

    protected static $imageFields = [
        'cover'     => [
            // width to resize image after upload
            'width'       => 1920,

            // height to resize image after upload
            'height'      => 1080,

            // set true to crop image with the given width/height and you can also pass arr [x,y] coordinate for crop.
            'crop'        => false,

            // what disk you want to upload, default config('imageup.upload_disk')
            'disk'        => 'public',

            // a folder path on the above disk, default config('imageup.upload_directory')
            'path'        => 'blog-posts',

            // placeholder image if image field is empty
            'placeholder' => '/images/avatar-placeholder.svg',

            // validation rules when uploading image
            'rules'       => 'image|mimes:jpeg,jpg,png',

            // override global auto upload setting coming from config('imageup.auto_upload_images')
            'auto_upload' => false,

            // if request file is don't have same name, default will be the field name
            'file_input'  => 'file',

            // a hook that is triggered before the image is saved
            //'before_save' => BlurFilter::class,

            // a hook that is triggered after the image is saved
            //'after_save' => CreateWatermarkImage::class
        ],
        'thumbnail' => [
            // width to resize image after upload
            'width'       => 720,

            // height to resize image after upload
            'height'      => 405,

            // set true to crop image with the given width/height and you can also pass arr [x,y] coordinate for crop.
            'crop'        => true,

            // what disk you want to upload, default config('imageup.upload_disk')
            'disk'        => 'public',

            // a folder path on the above disk, default config('imageup.upload_directory')
            'path'        => 'blog-posts-thumbnail',

            // placeholder image if image field is empty
            'placeholder' => '/images/avatar-placeholder.svg',

            // validation rules when uploading image
            'rules'       => 'image|mimes:jpeg,jpg,png',

            // override global auto upload setting coming from config('imageup.auto_upload_images')
            'auto_upload' => false,

            // if request file is don't have same name, default will be the field name
            'file_input'  => 'file',

            // a hook that is triggered before the image is saved
            //'before_save' => BlurFilter::class,

            // a hook that is triggered after the image is saved
            //'after_save' => CreateWatermarkImage::class
        ],
    ];

    public function createdBy()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function updatedBy()
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

}
