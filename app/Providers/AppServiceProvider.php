<?php

namespace App\Providers;

use App\ApiClients\BattleNetClient;
use App\ApiClients\WarcraftLogsClient;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Laravel\Horizon\Horizon;
use Monolog\Formatter\LineFormatter;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        Horizon::auth(function ($request) {
            if (config('app.admin_battletag') == '') {
                return false;
            }

            return Auth::check() && Auth::user()->battletag == config('app.admin_battletag');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        $this->app->bind(Client::class, function ($app) {
            $stack = HandlerStack::create();

            $stack->push(\GuzzleHttp\Middleware::log(
                with(new \Monolog\Logger('api-consumer'))->pushHandler(
                    tap(new \Monolog\Handler\StreamHandler(storage_path('logs/api-consumer.log')), function ($handler) {
                        /** @var $handler \Monolog\Handler\StreamHandler */
                        $handler->setFormatter(
                            new LineFormatter(
                                null,
                                null,
                                true,
                                true
                            )
                        );
                    })
                ),
                new \GuzzleHttp\MessageFormatter(\GuzzleHttp\MessageFormatter::DEBUG)
            ));

            return new Client([
                'handler'         => $stack,

                // Float describing the number of seconds to wait while trying to connect to a server.
                // Use 0 to wait indefinitely (the default behavior).
                'connect_timeout' => 15,

                // Float describing the timeout to use when reading a streamed body
                'read_timeout'    => 15,

                // Float describing the timeout of the request in seconds.
                // Use 0 to wait indefinitely (the default behavior).
                'timeout'         => 30,
            ]);
        });

        $this->app->bind(BattleNetClient::class, function ($app) {
            /** @var $app \Illuminate\Contracts\Foundation\Application */
            return new BattleNetClient($app->make(Client::class));
        });

        $this->app->bind(WarcraftLogsClient::class, function ($app) {
            /** @var $app \Illuminate\Contracts\Foundation\Application */
            return new WarcraftLogsClient($app->make(Client::class));
        });
    }
}
