<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Character extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    protected $table = 'characters';

    protected $fillable = [
        'user_id',
        'name',
        'realm',
        'slug',
        'level',
        'class',
        'race',
        'spec',
        'avatar',
        'armory',
        'raider_io',
        'warcraft_logs',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
