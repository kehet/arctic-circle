<?php

namespace App\Jobs;

use App\ApiClients\WarcraftLogsClient;
use DateTime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;

class FetchLatestWarcraftLogs implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param \App\ApiClients\WarcraftLogsClient $client
     *
     * @return void
     */
    public function handle(WarcraftLogsClient $client)
    {
        $name = 'Arctic Circle';
        $realm = 'Darkspear';

        $zones = $client->getZones()->keyBy('id');

        $reports = $client->getGuildReports($name, $realm)
            ->take(5)
            ->map(function ($report) use ($zones, $client) {
                $zone = 'Tuntematon';

                if (isset($zones[$report->zone])) {
                    $zone = $zones[$report->zone]->name;
                }

                $start = Carbon::createFromTimestampMs($report->start);
                return (object)[
                    'id'    => $report->id,
                    'title' => $report->title,
                    'start' => $start->format(DateTime::ISO8601),
                    'zone'  => $zone,
                    'kills' => $this->parseKillsForBoss(
                        $client->getFights($report->id, ($start->isToday() || $start->isYesterday()))
                    ),
                ];
            });

        Cache::forever('warcraftlogs-parsed', $reports);
    }

    private function parseKillsForBoss(Collection $getFights): Collection
    {
        return $getFights
            ->filter(function ($fight) {
                return isset($fight->kill) && $fight->kill === true;
            })
            ->map(function ($fight) {
                $difficulty = '';

                switch ($fight->difficulty) {
                    case 1:
                        $difficulty = 'Lelumode';
                        break;
                    case 2:
                        $difficulty = 'Flex';
                        break;
                    case 3:
                        $difficulty = 'Normaali';
                        break;
                    case 4:
                        $difficulty = 'Heroic';
                        break;
                    case 5:
                        $difficulty = 'Mythic';
                        break;
                    case 10:
                        $difficulty = 'Dungeon';
                        break;
                    case 100:
                        $difficulty = 'FF/WildStar (wtf?)';
                        break;
                }

                return (object)[
                    'name'       => $fight->name,
                    'difficulty' => $difficulty,
                ];
            });
    }
}
