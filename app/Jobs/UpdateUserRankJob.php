<?php

namespace App\Jobs;

use App\ApiClients\BattleNetClient;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;

class UpdateUserRankJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @param \App\ApiClients\BattleNetClient $client
     *
     * @return void
     */
    public function handle(BattleNetClient $client)
    {
        $name = 'Arctic Circle';
        $realm = 'Darkspear';

        Cache::delete('api.guild.' . rawurlencode($name) . '.' . rawurlencode($realm) . '.members');

        $members = $client->guildMembers($name, $realm)
            ->map(function ($member) {
                return (object)[
                    'name'  => $member->character->name,
                    'realm' => $member->character->realm,
                    'slug'  => str_slug($member->character->name . '-' . $member->character->realm),
                    'rank'  => $member->rank,
                ];
            });

        $users = User::with('main')->get();

        foreach ($users as $user) {
            $isMember = false;

            foreach ($members as $member) {
                if (!empty($user->main) && $user->main->slug == $member->slug) {
                    $isMember = true;

                    if ($user->rank != $member->rank) {
                        Log::info('Member user=' . $user->id . ' name=' . $user->name . ' rank was changed from ' . $user->rank . ' to ' . $member->rank);
                        $user->update([
                            'rank' => $member->rank,
                        ]);
                    }

                    break;
                }
            }

            if (!$isMember && $user->rank != 99) {
                Log::info('Member user=' . $user->id . ' name=' . $user->name . ' rank was changed from ' . $user->rank . ' to 99');
                $user->update([
                    'rank' => 99,
                ]);
            }
        }
    }
}
