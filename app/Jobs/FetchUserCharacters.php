<?php

namespace App\Jobs;

use App\ApiClients\BattleNetClient;
use App\Character;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FetchUserCharacters implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var \App\User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param \App\ApiClients\BattleNetClient $client
     *
     * @return void
     */
    public function handle(BattleNetClient $client)
    {
        $classes = $client->dataClasses();
        $races = $client->dataRaces();
        $region = config('services.battlenet.region');

        $characters = $client->currentCharacters($this->user);

        foreach ($characters as $character) {
            $fallback = '?alt=wow/static/images/2d/avatar/' . $character->race . '-' . $character->gender . '.jpg';
            $avatar = 'https://render-' . $region . '.worldofwarcraft.com/character/' . $character->thumbnail . $fallback;

            $realmSpaces = str_replace(' ', '-', $character->realm);

            $armory = 'https://worldofwarcraft.com/en-gb/character/' . $realmSpaces . '/' . $character->name;
            $raiderIo = 'https://raider.io/characters/eu/' . $realmSpaces . '/' . $character->name;
            $warcraftLogs = null;

            // https://us.battle.net/forums/en/bnet/topic/20770657299
            if ($character->race === 25 || $character->race === 26) {
                $character->race = 24;
            }

            Character::updateOrInsert(
                [
                    'slug' => str_slug($character->name . '-' . $character->realm),
                ],
                [
                    'user_id'       => $this->user->id,
                    'name'          => $character->name,
                    'realm'         => $character->realm,
                    'guildSlug'     => (!empty($character->guild) ? str_slug($character->guild . '-' . $character->guildRealm) : null),
                    'guild'         => $character->guild ?? null,
                    'guildRealm'    => $character->guildRealm ?? null,
                    'level'         => $character->level,
                    'class'         => $character->class,
                    'className'     => optional($classes->where('id', $character->class)->first())->name,
                    'race'          => $character->race,
                    'raceName'      => optional($races->where('id', $character->race)->first())->name,
                    'spec'          => optional(optional($character)->spec)->name,
                    'avatar'        => $avatar,
                    'armory'        => $armory,
                    'raider_io'     => $raiderIo,
                    'warcraft_logs' => $warcraftLogs,
                ]
            );

        }
    }
}
