<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Application extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    protected $table = 'applications';

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function character()
    {
        return $this->hasOne(Character::class, 'id', 'character_id');
    }

    public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

}
