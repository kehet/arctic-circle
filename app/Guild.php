<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use OwenIt\Auditing\Contracts\Auditable;

class Guild extends Model implements Auditable
{

    use \OwenIt\Auditing\Auditable;

    use Notifiable;

}
