<?php

namespace App\Policies;

use App\Application;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ApplicationPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the application.
     *
     * @param  \App\User $user
     *
     * @return mixed
     */
    public function index(User $user)
    {
        return $user->is_member;
    }

    /**
     * Determine whether the user can view the application.
     *
     * @param  \App\User $user
     * @param  \App\Application $application
     *
     * @return mixed
     */
    public function view(User $user, Application $application)
    {
        return ($user->is_member || $user->id == $application->user_id);
    }

    /**
     * Determine whether the user can create applications.
     *
     * @param  \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the application.
     *
     * @param  \App\User $user
     * @param  \App\Application $application
     *
     * @return mixed
     */
    public function update(User $user, Application $application)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the application.
     *
     * @param  \App\User $user
     * @param  \App\Application $application
     *
     * @return mixed
     */
    public function comment(User $user, Application $application)
    {
        return ($user->is_member || $user->id == $application->user_id);
    }
}
