<?php

namespace App\Policies;

use App\BlogPost;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BlogPostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create blog posts.
     *
     * @param  \App\User $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the blog post.
     *
     * @param  \App\User $user
     * @param  \App\BlogPost $blogPost
     *
     * @return mixed
     */
    public function update(User $user, BlogPost $blogPost)
    {
        return $user->is_admin;
    }

    /**
     * Determine whether the user can update the blog post.
     *
     * @param  \App\User $user
     * @param  \App\BlogPost $blogPost
     *
     * @return mixed
     */
    public function destroy(User $user, BlogPost $blogPost)
    {
        return $user->is_admin;
    }
}
