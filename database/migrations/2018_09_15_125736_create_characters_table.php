<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCharactersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('characters', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->string('slug')->unique();
            $table->string('name');
            $table->string('realm');
            $table->string('guildSlug')->nullable();
            $table->string('guild')->nullable();
            $table->string('guildRealm')->nullable();
            $table->string('level');
            $table->string('class');
            $table->string('className');
            $table->string('race');
            $table->string('raceName');
            $table->string('spec')->nullable();
            $table->string('avatar');
            $table->string('armory')->nullable();
            $table->string('raider_io')->nullable();
            $table->string('warcraft_logs')->nullable();

            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('character_id')->nullable();
            $table->foreign('character_id')->references('id')->on('characters');
        });

        Schema::table('applications', function (Blueprint $table) {
            $table->unsignedInteger('character_id')->nullable();
            $table->foreign('character_id')->references('id')->on('characters');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('characters');
    }
}
