<?php

use Faker\Generator as Faker;

$factory->define(App\BlogPost::class, function (Faker $faker) {
    return [
        'title'   => $faker->words(mt_rand(1, 3), true),
        'body'    => $faker->realText(200),
    ];
});
